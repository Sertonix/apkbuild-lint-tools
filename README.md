# apkbuild-lint-tools

Docker container with the necessary linting tools for verifying APKBUILD files

## Tools

* **[atools](https://gitlab.alpinelinux.org/Leo/atools)** - Contains `apkbuild-lint`
    for general codestyle linting
* **[shellcheck](https://www.shellcheck.net/)** - A generic shell script linter,
    warning against common issues in shell scripts. Note that by default it's
    too strict for APKBUILD files.
* **apkbuild-shellcheck** - A wrapper script to use shellcheck on APKBUILD
    files. It uses a shim file that defines variabes normally defined by abuild
    and 'uses' them so that shellcheck does not warn about them, while still
    warning about other variables. It also disables some checks that are not
    applicable for APKBUILDs.

## Auxilary scripts

* **changed-aports** - Returns a list of changed APKBUILD files in a certain
    branch, give a base branch: `changed-aports <basebranch>`.
