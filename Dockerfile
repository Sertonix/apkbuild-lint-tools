FROM registry.alpinelinux.org/alpine/infra/docker/golang:latest AS gobuilder

# renovate: datasource=gitlab-releases depName=alpine/infra/atools-go registryUrl=https://gitlab.alpinelinux.org
ARG ATOOLS_GO_VERSION=v0.2.0

RUN <<EOF
set -e
cd /home/build
mkdir atools-go
wget -O- https://gitlab.alpinelinux.org/alpine/infra/atools-go/-/archive/$ATOOLS_GO_VERSION/atools-go-$ATOOLS_GO_VERSION.tar.gz \
    | tar xz --strip-components=1 --directory atools-go
cd atools-go
./configure --strip
redo
EOF

FROM alpine:edge

RUN apk add --no-cache abuild doas atools spdx-licenses-list git \
    && adduser -D lint

COPY --from=gobuilder --chmod=0755 /home/build/atools-go/apkbuild-lint /usr/local/bin/apkbuild-lint

COPY overlay/ /

RUN chmod 0600 /etc/doas.d/buildozer.conf

COPY --from=koalaman/shellcheck:v0.10.0 /bin/shellcheck /bin/shellcheck

USER lint

# Very common in aports, so disable it for now
ENV SKIP_AL31=1

CMD changed-aports master | lint
